#ifndef _WT588D_H_
#define _WT588D_H_

#include <Arduino.h>

#ifdef __SAM3X8E__
typedef volatile RwReg PortReg;
typedef uint32_t PortMask;
#else
typedef volatile uint8_t PortReg;
typedef uint8_t PortMask;
#endif


class WT588D {
    
 public:
    WT588D(uint8_t chipSelectPin, uint8_t serialClockPin,
           uint8_t serialDataPin);
    
    void  begin(void);
    
    void    setVolume(uint8_t volume);
    void    playSound(uint8_t playListNumber);
    void    startLoopSound(void);
    void    stopLoopSound(void);
    
 private:
    int8_t csPin, sclPin, sdaPin; // pin numbers
    PortReg *csport, *sclport, *sdaport;
    PortMask cspinmask, sclpinmask, sdapinmask;

    void     sendCommand(uint8_t cmd);
};

#endif // _WT588D_H_
