/******************************************************************************/
/*                                                                            */
/*   Furgalladas Schalter und Sensoren                                        */
/*                                                                            */
/*   This code is released under MIT license                                  */
/*                                                                            */
/******************************************************************************/


/** \file

   ecto1.ino - Ecto1 lights and sound


   <table border="0" cellspacing="0" cellpadding="0">
   <tr> <td> Source:   </td> <td> ecto1.ino          </td></tr>
   <tr> <td> Revision: </td> <td> 1.1                        </td></tr>
   <tr> <td> Status:   </td> <td> ACCEPTED                   </td></tr>
   <tr> <td> Author:   </td> <td> Miguel Carpacho            </td></tr>
   <tr> <td> Date:     </td> <td> 22-APRIL-2017 20:00:56   </td></tr>
   </table>

   \n
   <table border="0" cellspacing="0" cellpadding="0">
   <tr> <td> COMPONENT: </td> <td> Ecto1 </td></tr>
   <tr> <td> TARGET:    </td> <td> Arduino </td></tr>
   </table>
*/

/*MIT License

Copyright (c) [2017] [M.Carpacho]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


#include "WT588D_threeLines.h"

//Define all lights
#define STROBELIGHT1 0
#define STROBELIGHT2 1
#define WINDSCREENLIGHT 2
#define ROOFLIGHTLEFTLIGHT 3
#define ROOFLIGHTRIGHTLIGHT 4
#define ROOFREDLIGHT 5
#define SIRENLIGHT1 6
#define SIRENLIGHT2 7
#define SIRENLIGHT3 8
#define SIRENLIGHT4 9

// set the correct pin connections for the WT588D chip
#define WT588D_SCL 10  //Module pin "P03" or pin # 10
#define WT588D_CS 11   //Module pin "P02" or pin # 11
#define WT588D_SDA 12  //Module pin "P01" or pin # 12
WT588D ecto1WT588D(WT588D_CS, WT588D_SCL, WT588D_SDA);
//Modes
#define SIREN 0
#define SOUNDTRACK 1
boolean startUpSequence = false;
byte mode;

void setup() {
  //Configure all LED pins
  pinMode(STROBELIGHT1, OUTPUT);
  pinMode(STROBELIGHT2, OUTPUT);
  pinMode(WINDSCREENLIGHT, OUTPUT);
  pinMode(ROOFLIGHTLEFTLIGHT, OUTPUT);
  pinMode(ROOFLIGHTRIGHTLIGHT, OUTPUT);
  pinMode(ROOFREDLIGHT, OUTPUT);
  pinMode(SIRENLIGHT1, OUTPUT);
  pinMode(SIRENLIGHT2, OUTPUT);
  pinMode(SIRENLIGHT3, OUTPUT);
  pinMode(SIRENLIGHT4, OUTPUT);
  digitalWrite(STROBELIGHT1, LOW);
  digitalWrite(STROBELIGHT2, LOW);
  digitalWrite(WINDSCREENLIGHT, LOW);
  digitalWrite(ROOFLIGHTLEFTLIGHT, LOW);
  digitalWrite(ROOFLIGHTRIGHTLIGHT, LOW);
  digitalWrite(ROOFREDLIGHT, LOW);
  digitalWrite(SIRENLIGHT1, LOW);
  digitalWrite(SIRENLIGHT2, LOW);
  digitalWrite(SIRENLIGHT3, LOW);
  digitalWrite(SIRENLIGHT4, LOW);
  
  uint16_t modeValue;
  ecto1WT588D.begin();
  modeValue = analogRead(0);
  if (modeValue>512){
    mode = SOUNDTRACK;
  }else{
    mode = SIREN;
  }
}

void loop() {
  if (startUpSequence == false){
    startUpSequence = true;
    delay(1500);
    //Hacemos la secuencia de arranque
    if (mode == SOUNDTRACK){
      ecto1WT588D.playSound(mode);
    }    
    digitalWrite(STROBELIGHT1, HIGH);
    delay(500);
    digitalWrite(STROBELIGHT2, HIGH);
    delay(1000);
    digitalWrite(WINDSCREENLIGHT, HIGH);
    delay(1000);
    digitalWrite(ROOFLIGHTLEFTLIGHT, HIGH);
    delay(500);
    digitalWrite(ROOFLIGHTRIGHTLIGHT, HIGH);
    delay(1500);
    digitalWrite(ROOFREDLIGHT, HIGH);
    delay(1000);
    digitalWrite(SIRENLIGHT1, HIGH);
    delay(500);
    digitalWrite(SIRENLIGHT2, HIGH);
    delay(500);
    digitalWrite(SIRENLIGHT3, HIGH);
    delay(500);
    digitalWrite(SIRENLIGHT4, HIGH);
    delay(3200);
    if (mode == SIREN){
      ecto1WT588D.playSound(mode);  
    }
    //Shut down all leds
    digitalWrite(STROBELIGHT1, LOW);
    digitalWrite(STROBELIGHT2, LOW);
    digitalWrite(WINDSCREENLIGHT, LOW);
    digitalWrite(ROOFLIGHTLEFTLIGHT, LOW);
    digitalWrite(ROOFLIGHTRIGHTLIGHT, LOW);
    digitalWrite(ROOFREDLIGHT, LOW);
    digitalWrite(SIRENLIGHT1, LOW);
    digitalWrite(SIRENLIGHT2, LOW);
    digitalWrite(SIRENLIGHT3, LOW);
    digitalWrite(SIRENLIGHT4, LOW);    
  }
  /*
  digitalWrite(WINDSCREENLIGHT, LOW);
  digitalWrite(ROOFLIGHTLEFTLIGHT, LOW);
  digitalWrite(ROOFLIGHTRIGHTLIGHT, LOW);
  digitalWrite(ROOFREDLIGHT, LOW);
  */
  digitalWrite(ROOFREDLIGHT, HIGH);
  digitalWrite(WINDSCREENLIGHT, HIGH);
  digitalWrite(SIRENLIGHT4, LOW); 
  digitalWrite(SIRENLIGHT1, HIGH);
  digitalWrite(ROOFLIGHTLEFTLIGHT, HIGH);
  digitalWrite(STROBELIGHT1, HIGH);
  digitalWrite(STROBELIGHT2, LOW);
  delay(50);
  digitalWrite(STROBELIGHT1, LOW);
  digitalWrite(STROBELIGHT2, HIGH);
  delay(50);
  digitalWrite(STROBELIGHT1, HIGH);
  digitalWrite(STROBELIGHT2, LOW);
  delay(50);
  digitalWrite(STROBELIGHT1, LOW);
  digitalWrite(STROBELIGHT2, HIGH);
  delay(50);
  digitalWrite(STROBELIGHT2, LOW);
  delay(50);
  digitalWrite(SIRENLIGHT1, LOW);
  digitalWrite(SIRENLIGHT2, HIGH);
  delay(250);
  digitalWrite(ROOFLIGHTLEFTLIGHT, LOW);
  digitalWrite(ROOFLIGHTRIGHTLIGHT, HIGH);
  digitalWrite(SIRENLIGHT2, LOW);
  digitalWrite(SIRENLIGHT3, HIGH);  
  delay (250);
  digitalWrite(SIRENLIGHT3, LOW);  
  digitalWrite(SIRENLIGHT4, HIGH);
  digitalWrite(ROOFREDLIGHT, LOW);
  digitalWrite(WINDSCREENLIGHT, LOW);
  delay (250);
  digitalWrite(ROOFLIGHTRIGHTLIGHT, LOW);
}
