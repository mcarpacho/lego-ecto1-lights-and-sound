Version of Lego Ecto1 modified with lights and sound. 

It makes use of WT588D library (https://github.com/ACDCLabs/WT588D), modified to 
get use of only three lines.

More info about the project: http://furgalladas.com/blog/?p=468